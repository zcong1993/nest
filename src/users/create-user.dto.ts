import { IsString, IsEmail, Max, Min, Length } from 'class-validator'

export class CreateUserDto {
  @Length(6, 20)
  readonly username: string

  @IsEmail()
  readonly email: string

  @Length(6, 20)
  readonly password: string
}
