import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { User } from './users.entity'
import { CreateUserDto } from './create-user.dto'

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  findAll(): Promise<User[]> {
    return this.userRepository.find()
  }

  async create(user: CreateUserDto) {
    const u = new User()

    u.username = user.username
    u.password = user.password + 'xsxs'
    u.email = user.email
    u.isActive = false

    await this.userRepository.save(u)
  }
}
