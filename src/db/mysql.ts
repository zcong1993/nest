import { TypeOrmModule } from '@nestjs/typeorm'

export const MysqlModule = TypeOrmModule.forRoot({
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: process.env.MYSQL_PASSWORD || 'root',
    database: 'nest',
    entities: [__dirname + '/../**/*.entity{.ts,.js}'],
    synchronize: true,
})
