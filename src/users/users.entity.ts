import { Entity, Column, PrimaryGeneratedColumn, Index } from 'typeorm'
import { Exclude } from 'class-transformer'

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    @Index({ unique: true })
    username: string

    @Column()
    @Index({ unique: true })
    email: string

    @Column()
    @Exclude()
    password: string

    @Column()
    isActive: boolean
}
