import * as helmet from 'helmet'
import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { ValidationPipe } from './validation-pipe/validation-pipe.pipe'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  app.use(helmet())
  app.enableCors()
  app.useGlobalPipes(new ValidationPipe())
  await app.listen(3000)
}
bootstrap()
