import { Controller, Get, UseInterceptors, ClassSerializerInterceptor,
  Body, Post, BadRequestException, ServiceUnavailableException,
} from '@nestjs/common'
import { UsersService } from './users.service'
import { CreateUserDto } from './create-user.dto'

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @UseInterceptors(ClassSerializerInterceptor)
  @Get()
  findAll() {
    return this.usersService.findAll()
  }

  @Post()
  async create(@Body() createUserDto: CreateUserDto) {
    try {
      await this.usersService.create(createUserDto)
    } catch (err) {
      if (/Duplicate entry/.test(err.message)) {
        throw new BadRequestException('user already exists')
      } else {
        throw new ServiceUnavailableException()
      }
    }
  }
}
