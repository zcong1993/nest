import { Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { CatsModule } from './cats/cats.module'
import { UsersModule } from './users/users.module'
import { MysqlModule } from './db/mysql'

@Module({
  imports: [MysqlModule, CatsModule, UsersModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
